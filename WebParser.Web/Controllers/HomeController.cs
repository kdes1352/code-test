﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using WebParser.Web.Models;
using HtmlDocument = HtmlAgilityPack.HtmlDocument;

namespace WebParser.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(UrlModel model)
        {
            var returnModel = new ReturnModel();
            if (ModelState.IsValid)
            {
                try
                {
                    var doc = GetHtmlDocument(model.Url);
                    returnModel.Images = GetImages(doc);
                    returnModel.WordCount = GetWordCount(doc);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("error", "An error occurred: " + ex.Message);
                }
            }
            return View(returnModel);
        }

        private static WordCountModel GetWordCount(HtmlDocument doc)
        {
            var returnModel = new WordCountModel
            {
                AllWords = new List<string>()
            };

            foreach (
                var node in
                doc.DocumentNode.SelectSingleNode("//body")
                    .SelectNodes("//text()")
                    .Where(n => n.ParentNode.Name != "script" && n.ParentNode.Name != "style"))
            {
                if (!string.IsNullOrWhiteSpace(node.InnerText))
                {
                    // split each selection of text into single words by whitespace and punctuation
                    var punctuation = node.InnerText.Where(char.IsPunctuation).Distinct().ToArray();
                    var theWords =
                        node.InnerText.Split()
                            .Select(x => x.Trim(punctuation))
                            .Where(a => !string.IsNullOrEmpty(a));
                    returnModel.AllWords.AddRange(theWords);
                }
            }

            returnModel.MostCommon = returnModel.AllWords.GroupBy(x => x).Select(x => new MostCommon
            {
                Name = x.Key,
                Count = x.Count()
            }).OrderByDescending(x => x.Count).ToList();
            return returnModel;
        }

        private static List<string> GetImages(HtmlDocument doc)
        {
            var images = new List<string>();

            // select src attribute of each image on page
            var srcs = doc.DocumentNode.DescendantsAndSelf("img");

            // loop through each element
            foreach (var img in srcs)
            {
                // loop through each attribute of each element
                foreach (var attr in img.Attributes)
                {
                    // check for any tags that contain src. in my initial version of this, I only grabbed the src tag.  
                    // while testing using various websites, it was clear that some sites (espn for example),
                    // use the attribute "data-default-src" on all of their images so only using the src tag is incomplete.
                    if (attr.Name.Contains("src") && !string.IsNullOrEmpty(attr.Value))
                    {
                        images.Add(attr.Value);
                        // breaking from the loop to prevent multiple versions of the same image
                        break;
                    }
                }
            }
            return images;
        }

        private static HtmlDocument GetHtmlDocument(string modelUrl)
        {
            var client = new WebClient();
            string url;
            if (!modelUrl.Contains("http://") && !modelUrl.Contains("https://"))
            {
                url = "http://" + modelUrl;
            }
            else
            {
                url = modelUrl;
            }
            var source = client.DownloadString(new Uri(url));
            var doc = new HtmlDocument();
            doc.LoadHtml(source);
            return doc;
        }
    }
}