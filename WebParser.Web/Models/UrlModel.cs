﻿using System.ComponentModel.DataAnnotations;

namespace WebParser.Web.Models
{
    public class UrlModel
    {
        [Required(ErrorMessage = "Please enter a Url")]
        public string Url { get; set; }
    }
}