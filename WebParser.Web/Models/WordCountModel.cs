﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebParser.Web.Models
{
    public class WordCountModel
    {
        public List<string> AllWords { get; set; }
        public List<MostCommon> MostCommon { get; set; }
    }

    public class MostCommon
    {
        public string Name { get; set; }
        public int Count { get; set; }
    }
}