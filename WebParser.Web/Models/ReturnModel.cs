﻿using System.Collections.Generic;

namespace WebParser.Web.Models
{
    public class ReturnModel
    {
        public List<string> Images { get; set; }
        public WordCountModel WordCount { get; set; }
    }
}